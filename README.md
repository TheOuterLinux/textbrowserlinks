# TextBrowserLinks
A list of text-browser safe websites and tips.
### Note:
**I prefer w3m for most text-based web browsing.** But why use a text-browser? Text-browsers are very fast, no pop-ups or ad banners, and fewer scripts running to threaten your privacy.
### Tips:
* I highly recommend you set your text-browser's user-agent to mobile:
>> Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1
* I also recommend duplicating the original ~/.w3m/config file as "config_Desktop" so if you do get JavaScript errors and aren't on a video or internet radio site, it shouldn't happen as much:
>> * **Example:** "w3m -config .w3m/config_Desktop www.slashdot.org"
>> * Me personally, I set an alias in my $HOME/.bashrc file as: *alias w3m-desktop="w3m -config $HOME/.w3m/config_Desktop"*. This way, I can just type "w3m-desktop [URL]" when I need to.
* If using w3m, change your external web browser:
>> * **For X:** "*mpv -ytdl --vo=opengl,x11 --ao=pulse,alsa*"
>> * **For TTY:** "*mpv -ydtl --vo=drm,tct --ao=pulse,alsa*"
>> * **You could also just do:** "*mpv --vo=opengl,drm,x11,sdl,tct,caca --ao=alsa,pulse --ytdl-format='best[ext=mp4][height<=?720]'*" and mpv will cycle through each vo and ao until something works and that applies to both GUI and TTY
>> * **For Raspberry Pi users wihout rpi as vo:** *mpv --vo=x11,tct --ao=pulse,alsa --ytdl-format="best[ext=mp4][height<=?720]"*
* Using mpv in the tip above will allow you to open URLs using mpv and therefore, you will be able to open URL's supported by [youtub-dl](https://rg3.github.io/youtube-dl/supportedsites.html) because the mobile user agent essentially tells the website to load the MP4 version instead of FLV for video sites. If you do get a JavaScript error, ignore it and try SHIFT+M to load the external browser (mpv) anyway since mpv uses youtube-dl by default and parse the URL for you for supported formats.
>> * If there is no audio or only plays in headphones, try "pulseaudio --start"
>> * If you are using HDMI for audio and in TTY but want to use headphones with pulseaudio running, use mpv without the "--ao=alsa" option.
* Running "*w3m .*" or "*w3m $HOME*" will open /home as a very limited file manager, but should still be able to view images.
* I prefer NOT to have *w3m-img* or *w3m-inline-image* installed because it is just super annoying and you should be able to press SHIFT+I to view images (the green links) whenever you need to.
* Installing gpm will give you a mouse for TTY that also copies (highlight text with left-click) and pastes (right-click); if in TTY, press Esc while running w3m to use it.
>> * If it isn't running at startup, try "sudo service gpm start"
>> * You can use "gpm -t --help" to list types of mice available, including touch screen ones.
* If you need to lock the screen while in w3m but don't want to open another TTY or quit the current session, you can use "!" to open a shell and type "vlock." You will need to install vlock, of course.
>> * This shortcut also helps if you try to parse the current URL with the external browser mpv idea mentioned, but doesn't find the mp4 file automatically. Use "u" over the link to display the mp4 URL and then use gpm to copy and paste into the shell.
